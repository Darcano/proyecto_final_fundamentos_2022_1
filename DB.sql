-- Database: fundamentos

-- DROP DATABASE IF EXISTS fundamentos;

CREATE DATABASE fundamentos
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'es_CO.UTF-8'
    LC_CTYPE = 'es_CO.UTF-8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;
    
-- SCHEMA: public

-- DROP SCHEMA IF EXISTS public ;

CREATE SCHEMA IF NOT EXISTS public
    AUTHORIZATION postgres;

COMMENT ON SCHEMA public
    IS 'standard public schema';

GRANT ALL ON SCHEMA public TO PUBLIC;

GRANT ALL ON SCHEMA public TO postgres;

-- TABLE: mascota

CREATE TABLE public.mascota
(
    id_mascota integer NOT NULL,
    nombre text NOT NULL,
    color text NOT NULL,
    sexo text NOT NULL,
    PRIMARY KEY (id_mascota)
);

ALTER TABLE IF EXISTS public.mascota
    OWNER to postgres;
    
-- TABLE: propietario
    
CREATE TABLE public.propietario
(
    cedula integer NOT NULL,
    nombre text NOT NULL,
    PRIMARY KEY (cedula)
);

ALTER TABLE IF EXISTS public.propietario
    OWNER to postgres;
   
-- TABLE: parentesco 
    
CREATE TABLE public.parentesco
(
    mascota integer NOT NULL,
    propietario integer NOT NULL,
    PRIMARY KEY (mascota,propietario)
);

ALTER TABLE IF EXISTS public.parentesco
    OWNER to postgres;
	
ALTER TABLE public.parentesco
add constraint fk_parentesco_mascota
	FOREIGN key (mascota)
	REFERENCES public.mascota(id_mascota);
	
ALTER TABLE public.parentesco
add constraint fk_parentesco_propietario
	FOREIGN key (propietario)
	REFERENCES public.propietario(cedula);

-- INSERT: Datos de prueba

INSERT INTO public.mascota(
	id_mascota, nombre, color, sexo)
	VALUES (1, 'Azabache', 'Negro', 'Macho'),(2, 'Artemis', 'Blanco', 'Macho')
	,(3, 'Kenji', 'Blanco', 'Macho');
	
INSERT INTO public.propietario(
	cedula, nombre)
	VALUES (1, 'Stivel Pinilla'),(2, 'Leidy Quiroga'),(3,'Segio Cardenas');
	
INSERT INTO public.parentesco(
	mascota, propietario)
	VALUES (1, 1),(2, 2),(3,3);

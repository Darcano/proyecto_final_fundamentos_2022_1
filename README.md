# Requisitos
- Descargar PgAdmin (Herramienta para gestionar y administrar PostgreSQL) : https://www.pgadmin.org/download/

- En su defento instalar solo PostgreSQL : https://www.postgresql.org/download/

- Instalr NodeJS: https://nodejs.org/es/download/

# Ejecutar

- Entrar por consola a PostgreSQL y colcoarle contraseña al usuario **postgres**. Con el siguiente comando: `ALTER USER postgres WITH password '123456';
`

- Ejecutar el archivo **DB.sql** en pgAdmin o en su defento en la consola de PostgreSQL.


- Correr `npm start` en la carpeta final_node y en el acarpeta final_react por separado.


## Comandos utiles

Entrar por consola a Postgres: `sudo -u postgres psql`

Ingresar al ususario postgres: `sudo su - postgres`

Ingresar a la base de datos (Previamente con el usuario): `psql -d database_name`
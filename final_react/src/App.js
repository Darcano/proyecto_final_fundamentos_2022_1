import React, { useState, useEffect } from "react";
import "./App.css";

function App() {
  const [prueba, setPrueba] = useState();
  const [mascota, setMascota] = useState();
  const [propietario, setPropietario] = useState();
  const [parentesco, setParentesco] = useState();

  useEffect(() => {
    getPrueba();
    getMascota();
    getPropietario();
    getParentesco();
  }, []);

  //Funciones para las peticiones de PRUEBA

  function getPrueba() {
    fetch("http://localhost:3001/prueba")
      .then((response) => {
        return response.text();
      })
      .then((data) => {
        setPrueba(data);
      });
  }

  function createPrueba() {
    let id = prompt("Ingrese el ID");
    let nombre = prompt("Ingrese el Nombre");

    fetch("http://localhost:3001/prueba", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ id, nombre }),
    })
      .then((response) => {
        return response.text();
      })
      .then((data) => {
        alert(data);
        getPrueba();
      });
  }

  function deletePrueba() {
    let id = prompt("Enter merchant id");

    fetch(`http://localhost:3001/prueba/${id}`, {
      method: "DELETE",
    })
      .then((response) => {
        return response.text();
      })
      .then((data) => {
        alert(data);
        getPrueba();
      });
  }

  //Funciones para las peticiones de MASCOTA

  function getMascota() {
    fetch("http://localhost:3001/mascota")
      .then((response) => {
        return response.text();
      })
      .then((data) => {
        setMascota(data);
      });
  }

  function createMascota() {
    let auxArray = [];
    auxArray = JSON.parse(mascota);
    let id_mascota = auxArray.length + 1;
    if (id_mascota) {
      let nombre = prompt("Ingrese el Nombre");
      if (nombre) {
        let color = prompt("Ingrese el Color");
        if (color) {
          let sexo = prompt("Ingrese el Sexo");
          if (sexo) {
            fetch("http://localhost:3001/mascota", {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify({ id_mascota, nombre, color, sexo }),
            })
              .then((response) => {
                return response.text();
              })
              .then((data) => {
                alert(data);
                getMascota();
              });
          }
        }
      }
    }
  }

  function deleteMascota(id) {
    if (window.confirm("¿Desea eliminar la Mascota?")) {
      fetch(`http://localhost:3001/parentesco/mascota/delete/${id}`, {
        method: "DELETE",
      })
        .then((response) => {
          return response.text();
        })
        .then((data) => {
          alert(data);
          getParentesco();
        });
      fetch(`http://localhost:3001/mascota/${id}`, {
        method: "DELETE",
      })
        .then((response) => {
          return response.text();
        })
        .then((data) => {
          alert(data);
          getMascota();
        });
    }
  }

  //Funciones para las peticiones de PROPIETARIO

  function getPropietario() {
    fetch("http://localhost:3001/propietario")
      .then((response) => {
        return response.text();
      })
      .then((data) => {
        setPropietario(data);
      });
  }

  function createPropietario() {
    let auxArray = [];
    auxArray = JSON.parse(propietario);
    let cedula = auxArray.length + 1;
    let nombre = prompt("Ingrese el Nombre");
    if (nombre) {
      fetch("http://localhost:3001/propietario", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ cedula, nombre }),
      })
        .then((response) => {
          return response.text();
        })
        .then((data) => {
          alert(data);
          getPropietario();
        });
    }
  }

  function deletePropietario(cedula) {
    if (window.confirm("¿Desea Eliminar el Propietario?")) {
      fetch(`http://localhost:3001/parentesco/propietario/delete/${cedula}`, {
        method: "DELETE",
      })
        .then((response) => {
          return response.text();
        })
        .then((data) => {
          alert(data);
          getParentesco();
        });
      fetch(`http://localhost:3001/propietario/${cedula}`, {
        method: "DELETE",
      })
        .then((response) => {
          return response.text();
        })
        .then((data) => {
          alert(data);
          getPropietario();
        });
    }
  }

  //Funciones para las peticiones de PARENTESCO

  function getParentesco() {
    fetch("http://localhost:3001/parentesco")
      .then((response) => {
        return response.text();
      })
      .then((data) => {
        setParentesco(data);
      });
  }

  function createParentesco() {
    let mascota = prompt("Ingrese el ID de la Mascota");
    let propietario = prompt("Ingrese el ID del propietario");

    fetch("http://localhost:3001/parentesco", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ mascota, propietario }),
    })
      .then((response) => {
        return response.text();
      })
      .then((data) => {
        alert(data);
        getParentesco();
      });
  }

  function deleteParentesco(mascota, propietario) {
    fetch(`http://localhost:3001/parentesco/${mascota}/${propietario}`, {
      method: "DELETE",
    })
      .then((response) => {
        return response.text();
      })
      .then((data) => {
        alert(data);
        getParentesco();
      });
  }

  return (
    <div className="panel">
      <div className="titulo_bloque">Mascotas</div>
      <div className="titulo_bloque">Propietarios</div>
      <div className="titulo_bloque">Parentescos</div>
      <div className="contenedor">
        <div className="cajon_mascota_titulo">
          <div>NOMBRE</div>
          <div>SEXO</div>
          <div>COLOR</div>
          <div></div>
        </div>
        {mascota
          ? JSON.parse(mascota).map((element) => {
              return (
                <div className="cajon_mascota">
                  <div>{element.nombre}</div>
                  <div>{element.sexo}</div>
                  <div>{element.color}</div>
                  <button
                    className="button-53"
                    id={"delete_mascota_" + element.id_mascota}
                    onClick={(e) => {
                      deleteMascota(element.id_mascota);
                    }}
                  >
                    Eliminar
                  </button>
                </div>
              );
            })
          : "No existen valor para Mascota"}
        <div>
          <button className="button-54" onClick={createMascota}>
            Agregar
          </button>
        </div>
      </div>

      <div className="contenedor">
        <div className="cajon_propietario_titulo">
          <div>NOMBRE</div>
          <div></div>
        </div>
        {propietario
          ? JSON.parse(propietario).map((element) => {
              return (
                <div className="cajon_propietario">
                  <div>{element.nombre}</div>
                  <button
                    className="button-53"
                    id={"delete_propietario_" + element.cedula}
                    onClick={(e) => {
                      deletePropietario(element.cedula);
                    }}
                  >
                    Eliminar
                  </button>
                </div>
              );
            })
          : "No existen valor para Propietario"}
        <div>
          <button className="button-54" onClick={createPropietario}>
            Agregar
          </button>
        </div>
      </div>
      <div className="contenedor">
        <div className="cajon_parentesco_titulo">
          <div>MASCOTA</div>
          <div>PROPIETARIO</div>
          <div></div>
        </div>
        {parentesco
          ? JSON.parse(parentesco).map((element) => {
              return (
                <div className="cajon_parentesco">
                  <div>{element.mascota}</div>
                  <div>{element.propietario}</div>
                  <button
                    className="button-53"
                    id={"delete_parentesco_" + element.cedula}
                    onClick={(e) => {
                      deleteParentesco(element.id_mascota, element.cedula);
                    }}
                  >
                    Eliminar
                  </button>
                </div>
              );
            })
          : "No existen valor para Parentesco"}
        <div>
          <button className="button-54" onClick={createParentesco}>
            Agregar
          </button>
        </div>
      </div>
    </div>
  );
}

export default App;

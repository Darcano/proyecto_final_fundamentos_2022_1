const Pool = require("pg").Pool;

const pool = new Pool({
  user: "postgres",
  host: "127.0.0.1",
  database: "fundamentos",
  password: "123456",
  port: 5432,
});

const getPrueba = () => {
  return new Promise(function (resolve, reject) {
    pool.query("SELECT * FROM prueba ORDER BY id ASC", (error, results) => {
      if (error) {
        reject(error);
      }
      resolve(results.rows);
    });
  });
};

const createPrueba = (body) => {
  return new Promise(function (resolve, reject) {
    const { id, nombre } = body;

    pool.query(
      "INSERT INTO prueba (id, nombre) VALUES ($1, $2) RETURNING *",
      [id, nombre],
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(
          `Se ha agragado un Nuevo Elemento: ${JSON.stringify(results.rows[0])}`
        );
      }
    );
  });
};

const deletePrueba = (merchantId) => {
  return new Promise(function (resolve, reject) {
    const id = parseInt(merchantId);

    pool.query("DELETE FROM prueba WHERE id = $1", [id], (error, results) => {
      if (error) {
        reject(error);
      }
      resolve(`Se ha eliminado el Elemento ID: ${id}`);
    });
  });
};

const getMascota = () => {
  return new Promise(function (resolve, reject) {
    pool.query(
      "SELECT * FROM mascota ORDER BY id_mascota ASC",
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(results.rows);
      }
    );
  });
};

const createMascota = (body) => {
  return new Promise(function (resolve, reject) {
    const { id_mascota, nombre, color, sexo } = body;

    pool.query(
      "INSERT INTO mascota (id_mascota, nombre, color, sexo) VALUES ($1, $2, $3, $4) RETURNING *",
      [id_mascota, nombre, color, sexo],
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(
          `Se ha agragado una nueva Mascota: ${JSON.stringify(results.rows[0])}`
        );
      }
    );
  });
};

const deleteMascota = (mascotaID) => {
  return new Promise(function (resolve, reject) {
    const id = parseInt(mascotaID);

    pool.query(
      "DELETE FROM mascota WHERE id_mascota = $1",
      [id],
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(`Se ha eliminado la Mascota ID: ${id}`);
      }
    );
  });
};

const getPropietario = () => {
  return new Promise(function (resolve, reject) {
    pool.query(
      "SELECT * FROM propietario ORDER BY cedula ASC",
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(results.rows);
      }
    );
  });
};

const createPropietario = (body) => {
  return new Promise(function (resolve, reject) {
    const { cedula, nombre } = body;

    pool.query(
      "INSERT INTO propietario (cedula, nombre) VALUES ($1, $2) RETURNING *",
      [cedula, nombre],
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(
          `Se ha agragado un nuevo Propietario: ${JSON.stringify(
            results.rows[0]
          )}`
        );
      }
    );
  });
};

const deletePropietario = (propietarioID) => {
  return new Promise(function (resolve, reject) {
    const id = parseInt(propietarioID);

    pool.query(
      "DELETE FROM propietario WHERE cedula = $1",
      [id],
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(`Se ha eliminado el propietario Cedula: ${id}`);
      }
    );
  });
};

const getParentesco = () => {
  return new Promise(function (resolve, reject) {
    pool.query(
      "SELECT mascota.id_mascota, mascota.nombre AS Mascota, propietario.cedula, propietario.nombre AS Propietario " +
        "FROM mascota, propietario, parentesco " +
        "WHERE mascota.id_mascota = parentesco.mascota " +
        "AND propietario.cedula = parentesco.propietario",
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(results.rows);
      }
    );
  });
};

const getParentescoMascota = (MascotaID) => {
  return new Promise(function (resolve, reject) {
    const id = parseInt(MascotaID);

    pool.query(
      "SELECT * FROM parentesco WHERE mascota = $1",
      [id],
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(results.rows);
      }
    );
  });
};

const getParentescoPropietario = (PropietarioID) => {
  return new Promise(function (resolve, reject) {
    const id = parseInt(PropietarioID);

    pool.query(
      "SELECT * FROM parentesco WHERE propietario = $1",
      [id],
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(results.rows);
      }
    );
  });
};

const createParentesco = (body) => {
  return new Promise(function (resolve, reject) {
    const { mascota, propietario } = body;

    pool.query(
      "INSERT INTO parentesco (mascota, propietario) VALUES ($1, $2) RETURNING *",
      [mascota, propietario],
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(
          `Se ha agragado un nuevo Parentesco: ${JSON.stringify(
            results.rows[0]
          )}`
        );
      }
    );
  });
};

const deleteParentesco = (mascotaID, propietarioID) => {
  return new Promise(function (resolve, reject) {
    const mascota = parseInt(mascotaID);
    const propietario = parseInt(propietarioID);

    pool.query(
      "DELETE FROM parentesco WHERE mascota = $1 and propietario = $2",
      [mascota, propietario],
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(`Se ha eliminado el parentesco`);
      }
    );
  });
};

const deleteParentescoMascota = (mascotaID) => {
  return new Promise(function (resolve, reject) {
    const mascota = parseInt(mascotaID);

    pool.query(
      "DELETE FROM parentesco WHERE mascota = $1",
      [mascota],
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(`Se ha eliminado el parentesco`);
      }
    );
  });
};

const deleteParentescoPropietario = (propietarioID) => {
  return new Promise(function (resolve, reject) {
    const propietario = parseInt(propietarioID);

    pool.query(
      "DELETE FROM parentesco WHERE propietario = $1",
      [propietario],
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(`Se ha eliminado el parentesco`);
      }
    );
  });
};

/**
 * const deletePropietario = (propietarioID) => {
  return new Promise(function (resolve, reject) {
    const id = parseInt(propietarioID);

    pool.query(
      "DELETE FROM propietario WHERE cedula = $1",
      [id],
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(`Se ha eliminado el propietario Cedula: ${id}`);
      }
    );
  });
};
 */

module.exports = {
  getPrueba,
  createPrueba,
  deletePrueba,
  getMascota,
  createMascota,
  deleteMascota,
  getPropietario,
  createPropietario,
  deletePropietario,
  getParentesco,
  createParentesco,
  deleteParentesco,
  deleteParentescoMascota,
  deleteParentescoPropietario,
  getParentescoMascota,
  getParentescoPropietario,
};

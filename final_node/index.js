const express = require("express");
const app = express();
const port = 3001;
const {
  getPrueba,
  createPrueba,
  deletePrueba,
  getMascota,
  createMascota,
  deleteMascota,
  getPropietario,
  createPropietario,
  deletePropietario,
  getParentesco,
  createParentesco,
  deleteParentesco,
  deleteParentescoMascota,
  deleteParentescoPropietario,
  getParentescoMascota,
  getParentescoPropietario,
} = require("./modelo.js");
app.use(express.json());
app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "http://localhost:3000");
  res.setHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Content-Type, Access-Control-Allow-Headers"
  );
  next();
});

//Peticiones para PRUEBA

app.get("/prueba", (req, res) => {
  getPrueba()
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

app.post("/prueba", (req, res) => {
  createPrueba(req.body)
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

app.delete("/prueba/:id", (req, res) => {
  deletePrueba(req.params.id)
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

//Peticiones para MASCOTA

app.get("/mascota", (req, res) => {
  getMascota()
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

app.post("/mascota", (req, res) => {
  createMascota(req.body)
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

app.delete("/mascota/:id", (req, res) => {
  deleteMascota(req.params.id)
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

//Peticiones para PROPIETARIO

app.get("/propietario", (req, res) => {
  getPropietario()
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

app.post("/propietario", (req, res) => {
  createPropietario(req.body)
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

app.delete("/propietario/:id", (req, res) => {
  deletePropietario(req.params.id)
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

//Peticiones para PARENTESCO

app.get("/parentesco", (req, res) => {
  getParentesco()
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

app.get("/parentesco/mascota/:mascota", (req, res) => {
  getParentescoMascota(req.params.mascota)
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

app.get("/parentesco/propietario/:propietario", (req, res) => {
  getParentescoPropietario(req.params.propietario)
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

app.post("/parentesco", (req, res) => {
  createParentesco(req.body)
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

app.delete("/parentesco/:mascota/:propietario", (req, res) => {
  deleteParentesco(req.params.mascota, req.params.propietario)
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

app.delete("/parentesco/mascota/delete/:mascota", (req, res) => {
  deleteParentescoMascota(req.params.mascota)
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
});

app.delete("/parentesco/propietario/delete/:propietario", (req, res) => {
  deleteParentescoPropietario(req.params.propietario)
    .then((response) => {
      res.status(200).send(response);
    })
    .catch((error) => {
      res.status(500).send({ error: error, valor: req.params.propietario });
    });
});

app.listen(port, () => {
  console.log(`App running on port ${port}.`);
});
